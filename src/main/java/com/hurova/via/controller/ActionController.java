package com.hurova.via.controller;


import com.hurova.via.converter.algorithm.CSVConverter;
import com.hurova.via.converter.algorithm.JSONConverter;
import com.hurova.via.converter.algorithm.XMLConverter;
import com.hurova.via.converter.util.Converter;
import com.hurova.via.converter.util.ConverterManager;
import com.hurova.via.domain.Action;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;


@RestController
@RequestMapping("/action")
@Slf4j
public class ActionController {

    List<Action> list = new ArrayList<>();

    @PostMapping(value = "/create", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity getActionData(@RequestPart("type") String type, @RequestPart("data") byte[] data) {
        Converter<Action> converter = getConverter(type);
        log.info("The Action has been Retrieved.");
        log.info(converter.toObject(Action.class,data).toString());
        log.info(type);
        list.add(converter.toObject(Action.class,data));
        list.forEach(System.out::println);
        return ResponseEntity.ok().build();
    }


    private Converter<Action> getConverter(String type) {
        if (type != null) {
            switch (type) {
                case "XML":
                    return ConverterManager.getConverter(XMLConverter.class);
                case "JSON":
                    return ConverterManager.getConverter(JSONConverter.class);
                case "CSV":
                    return ConverterManager.getConverter(CSVConverter.class);
                default:
                    throw new IllegalArgumentException("This file type is not specified " + type + ".");
            }
        } else {
            throw new IllegalArgumentException("File type is not specified.");
        }
    }
}
