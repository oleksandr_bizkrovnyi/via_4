package com.hurova.via.domain;

import java.util.Map;

public interface MapConverter<T> {
    Map toMap();

    T fromMap(Map<String, String> paramValues);
}
