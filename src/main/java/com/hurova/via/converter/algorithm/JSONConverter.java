package com.hurova.via.converter.algorithm;

import com.google.gson.Gson;
import com.hurova.via.converter.util.Converter;
import com.hurova.via.domain.Action;
import org.apache.commons.io.input.CharSequenceReader;

import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;

public class JSONConverter implements Converter<Action> {

    @Override
    public Action toObject(Class<? extends Action> action, String path) {
        try {
            return new Gson().fromJson(readFile(path), action);
        } catch (IOException e) {
            System.out.println("Unable to read file");
            throw new IllegalArgumentException("File Url is broken");
        }
    }

    @Override
    public Action toObject(Class<? extends Action> claz, byte[] data) {
        Reader reader = new CharSequenceReader(new String(data));
        return new Gson().fromJson(reader, claz);
    }

    @Override
    public void fromObject(Action action) {
        System.out.println("Start generating JSON file");
        String result = new Gson().toJson(action);
        String path = System.getProperty("user.home") + "/Desktop/JSON_" + System.currentTimeMillis() + ".txt";
        try {
            Files.write(Paths.get(path), result.getBytes());
            System.out.println("Now you can find your file by this path: " + path);
        } catch (IOException e) {
            System.out.println("File saving is impossible");
        }
    }
}
