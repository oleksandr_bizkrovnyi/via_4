package com.hurova.via.converter.algorithm;

import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import com.hurova.via.converter.util.Converter;
import com.hurova.via.domain.Action;
import org.apache.commons.io.input.CharSequenceReader;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class CSVConverter implements Converter<Action> {


    @Override
    public Action toObject(Class<? extends Action> claz, String path) {
        try {
            Reader reader = new FileReader(path);
            return getActionFromReader(reader);
        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException("Unable to find file", e);
        }

    }

    @Override
    public Action toObject(Class<? extends Action> claz, byte[] data) {
        Reader reader = new CharSequenceReader(new String(data));
        return getActionFromReader(reader);
    }

    private Action getActionFromReader(Reader reader) {
        try {
            Iterator<Map<String, String>> iterator = new CsvMapper()
                    .readerFor(Map.class)
                    .with(CsvSchema.emptySchema().withHeader())
                    .readValues(reader);
            Map<String, String> keyVals = new HashMap<>();
            while (iterator.hasNext()) {
                keyVals = iterator.next();
            }
            return new Action().fromMap(keyVals);
        } catch (IOException e) {
            throw new IllegalArgumentException("Unable to find file", e);
        }
    }

    @Override
    public void fromObject(Action action) {
        try {
            System.out.println("Start generating CSV file");
            String path = System.getProperty("user.home") + "/Desktop/CSV_" + System.currentTimeMillis() + ".csv";
            System.out.println("Now you can find your file by this path: " + path);
            File tempFile = new File(path);
            FileOutputStream tempFileOutputStream = new FileOutputStream(tempFile);
            BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(tempFileOutputStream, 1024);
            OutputStreamWriter writerOutputStream = new OutputStreamWriter(bufferedOutputStream, StandardCharsets.UTF_8);
            List<Map<String, String>> mapList = new ArrayList<>();
            mapList.add(action.toMap());
            csvWriter(mapList, writerOutputStream);
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

    }

    private static void csvWriter(List<Map<String, String>> listOfMap, Writer writer) throws IOException {
        CsvSchema schema = null;
        CsvSchema.Builder schemaBuilder = CsvSchema.builder();
        if (listOfMap != null && !listOfMap.isEmpty()) {
            for (String col : listOfMap.get(0).keySet()) {
                schemaBuilder.addColumn(col);
            }
            schema = schemaBuilder.build().withLineSeparator("\r").withHeader();
        }
        CsvMapper mapper = new CsvMapper();
        mapper.writer(schema).writeValues(writer).writeAll(listOfMap);
        writer.flush();
    }
}
