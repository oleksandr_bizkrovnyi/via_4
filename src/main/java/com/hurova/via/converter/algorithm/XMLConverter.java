package com.hurova.via.converter.algorithm;


import com.hurova.via.converter.util.Converter;
import com.hurova.via.domain.Action;
import org.apache.commons.io.input.CharSequenceReader;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.io.Reader;

public class XMLConverter implements Converter<Action> {


    @Override
    public Action toObject(Class<? extends Action> claz, String path) {

        try {
            File file = new File(path);
            JAXBContext jaxbContext = JAXBContext.newInstance(claz);
            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            return (Action) jaxbUnmarshaller.unmarshal(file);
        } catch (JAXBException e) {
            throw new IllegalArgumentException(e);
        }

    }

    @Override
    public Action toObject(Class<? extends Action> claz, byte[] data) {
        try {
            Reader reader = new CharSequenceReader(new String(data));
            JAXBContext jaxbContext = JAXBContext.newInstance(claz);
            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            return (Action) jaxbUnmarshaller.unmarshal(reader);
        } catch (JAXBException e) {
            throw new IllegalArgumentException(e);
        }
    }


    @Override
    public void fromObject(Action action) {
        String path = System.getProperty("user.home") + "/Desktop/XML_" + System.currentTimeMillis() + ".xml";
        File file = new File(path);
        System.out.println("Start generating XML file");
        System.out.println("Now you can find your file by this path: " + path);
        JAXBContext jaxbContext = null;
        try {
            jaxbContext = JAXBContext.newInstance(Action.class);

            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
            // output pretty printed
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            jaxbMarshaller.marshal(action, file);
            jaxbMarshaller.marshal(action, System.out);
        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }
}
