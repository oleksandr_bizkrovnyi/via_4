package com.hurova.via.converter.util;

import com.hurova.via.converter.algorithm.CSVConverter;
import com.hurova.via.converter.algorithm.JSONConverter;
import com.hurova.via.converter.algorithm.XMLConverter;
import com.hurova.via.domain.Action;

public class ConverterManager {

    public static Converter<Action> getConverter(Class<? extends Converter> convType) {
        if (convType.equals(CSVConverter.class)) {
            return new CSVConverter();
        } else if (convType.equals(JSONConverter.class)) {
            return new JSONConverter();
        } else if (convType.equals(XMLConverter.class)) {
            return new XMLConverter();
        }
        throw new IllegalArgumentException("Converter type is not found");
    }
}
